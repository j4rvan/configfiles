#!/bin/bash

##############################
#       VOLUME
##############################

vol() {
	SINK="$(pamixer --get-default-sink | grep -v Default | awk '{print $1}')"
	VOLONOFF="$(pamixer --sink $SINK --get-mute)"
	volu="$(pamixer --sink $SINK --get-volume)"

	MuteIcon="+@fn=2;+@fn=0;"
	VolIcon="+@fn=2;+@fn=0;"

	if [ "$VOLONOFF" = "false" ]; then
		echo "$volu$VolIcon"
	else
		echo "$MuteIconMute"
	fi

}

##############################
#           Brightness
##############################

lume() {
	lume="$(xbacklight -get | awk -F'.' 'END{print $1}')"
	echo -e " $lume%"
}

##############################
#           System Temp
##############################

temp() {
	temp="$(sensors 2>/dev/null | awk '/Core 0/ {print $3+0}')"
	echo -e "+@fn=2;+@fn=0; $temp"℃""
}


########################################
#             NETWORK
########################################

network() {

	wifi="$(ip a | grep enp4s0 | grep inet | wc -l)"
	myssid="$(ip a | grep enp4s0 | grep inet | awk '{print substr($2, 1, length($2)-3)}')"

	if [ $wifi = 1 ]; then
		echo "+@fn=2; +@fn=0;$myssid"
	else
		echo "+@fn=2; +@fn=0; Disconnected"
	fi
}

########################################
#             DATE & TIME
########################################

clock() {

	timedate=$(date '+%b %e - %l:%M %p')
	echo "+@fn=2;+@fn=0; $timedate"

}

#########################################
#        Bar Action Output
#########################################

SLEEP_SEC=5
#loops forever outputting a line every SLEEP_SEC secs
while :; do
	echo "+@bg=1;|$(network) +@bg=4;| $(vol) +@bg=3;| $(clock)"
	sleep $SLEEP_SEC
done
