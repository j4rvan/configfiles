#! /bin/bash
DATA=$(cat /home/jarvan/.spectrwm_us.conf | awk '{split($0, a, "=")} {right=match(a[1],"]")} {left=match(a[1],"\\[")} { if (length(a[2])>0) printf "to %-15s press %-12s\n", substr(a[1], ++left, right-left), a[2]}' | sort)
echo "$DATA" | rofi -dmenu -p 'search keybind'
