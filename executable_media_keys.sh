# SINK="alsa_output.pci-0000_05_00.1.hdmi-stereo-extra1"

while getopts :tpn flag
do
    case "${flag}" in
        t) play="$(playerctl play-pause)";;
        p) prev="$(playerctl previous)";;
        n) next="$(playerctl next)";;
    esac
done