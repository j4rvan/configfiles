SINK="$(pamixer --get-default-sink | grep -v Default | awk '{print $1}')"

while getopts :idm flag
do
    case "${flag}" in
        i) inc="$(pamixer --sink $SINK -i 5)";;
        
        d) dec="$(pamixer --sink $SINK -d 5)";;
        m) mute="$(pamixer --sink $SINK -t)";;
    esac
done